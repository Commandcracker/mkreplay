from pynput.mouse import Listener as MouseListener
from pynput.keyboard import Listener as KeyboardListener
from pynput import keyboard
import pickle
import ansi_escape_sequences as ansi
import pprint
import time

t = 0
recording = []
record = False
debug = True


def save(key, args):
    if record:
        global t
        if t != 0:
            recording.append([key, time.time() - t, args])
        else:
            recording.append([key, 0, args])
        t = time.time()


def on_press(key):
    global record, recording
    if key == keyboard.Key.esc:
        if record:
            if debug:
                print(ansi.Foreground().BLUE + time.strftime("[%H:%M:%S] Debug:"))
                pprint.pprint(recording)
            name = str("Records/Record " + time.strftime("%H-%M-%S")) + ".p"
            pickle.dump(recording, open(name, "wb"))
            print(ansi.Foreground().GREEN + time.strftime('[%H:%M:%S] Saved Recording as "') + name + '"')
            record = False
            recording = []
        else:
            print(ansi.Foreground().GREEN + time.strftime("[%H:%M:%S] Recording"))
            record = True
    save(0, key)


def on_release(key):
    save(1, key)


def on_move(x, y):
    save(2, [x, y])


def on_click(x, y, button, pressed):
    if pressed:
        save(3, button)
    else:
        save(4, button)


def on_scroll(x, y, dx, dy):
    save(5, [dx, dy])


# Setup the listener threads
keyboard_listener = KeyboardListener(on_press=on_press, on_release=on_release)
mouse_listener = MouseListener(on_move=on_move, on_click=on_click, on_scroll=on_scroll)

# Start the threads and join them so the script doesn't end early
keyboard_listener.start()
mouse_listener.start()
keyboard_listener.join()
mouse_listener.join()
