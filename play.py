import pickle
import pprint
import ansi_escape_sequences as ansi
import time
from pynput.keyboard import Controller as KeyboardController
from pynput.mouse import Controller as MouseController

keyboard = KeyboardController()
mouse = MouseController()

recording = pickle.load(open("Records/" + "Record 18-27-53.p", "rb"))
print(ansi.Foreground().BLUE)
pprint.pprint(recording)
print(ansi.Foreground().GREEN)
time.sleep(2)
for item in recording:
    key = item[0]
    time.sleep(item[1])
    if key == 0:
        keyboard.press(item[2])
    elif key == 1:
        keyboard.release(item[2])
    elif key == 2:
        mouse.position = item[2][0], item[2][1]
    elif key == 3:
        mouse.press(item[2])
    elif key == 4:
        mouse.release(item[2])
    elif key == 5:
        mouse.scroll(item[2][0], item[2][1])
